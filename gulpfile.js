var gulp = require('gulp');
var sass = require('gulp-sass'); 

gulp.task('watch', ['sass'], function(){
  gulp.watch('styles/**/*.sass', ['sass']);
})

gulp.task('sass', function(){
  return gulp.src('styles/main.sass')
  .pipe(sass())
  .pipe(gulp.dest('styles'))
})